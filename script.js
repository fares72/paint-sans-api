const canvas = document.getElementById("app")
const colorInput = document.getElementById("color")
const sizeInput = document.getElementById("size")
const saveBtn = document.getElementById("save")

const ctx = canvas.getContext("2d")
ctx.fillStyle = "white";

let drawing = false;
let currentDrawing = {
    lineWidth: 1
}


function setColor(){
    currentDrawing.strokeColor = colorInput.value
    ctx.strokeStyle = colorInput.value
}
function setSize(){
    currentDrawing.lineWidth = sizeInput.value
    ctx.lineWidth = sizeInput.value
}


setColor()
setSize()


saveBtn.addEventListener("click", function(){
    const canvasImg = canvas.toDataURL("image/png")
    const link = document.createElement("a")
    link.setAttribute("download", "screnshot.png")
    link.href = canvasImg;
    link.click()
})

canvas.addEventListener("mousedown", function (event ){
    setSize()
    setColor()
    drawing = true;
    ctx.beginPath()
    ctx.moveTo(event.clientX, event.clientY)
})

canvas.addEventListener("mousemove", function (event){
    if(drawing){
        ctx.lineTo(event.clientX, event.clientY);
        ctx.stroke()
    }
})

window.addEventListener("mouseup", function (){
    drawing = false;
})

